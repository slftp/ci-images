FROM ubuntu:22.04 as windres
RUN apt-get update && \
    apt-get install -y --no-install-recommends mingw-w64

FROM ubuntu:22.04
WORKDIR /opt
COPY --from=windres /usr/bin/x86_64-w64-mingw32-windres /usr/bin/windres
ENV FPC_VERSION "3.2.2"
ENV PASDOC_VERSION "0.16.0"

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends git \
      libssl-dev zlib1g-dev \
      wget make perl gcc libc6-dev ca-certificates libncurses-dev && \
    # Download and install FPC
    wget -O fpc-${FPC_VERSION}.x86_64-linux.tar --quiet "https://sourceforge.net/projects/freepascal/files/Linux/${FPC_VERSION}/fpc-${FPC_VERSION}.x86_64-linux.tar/download" && \
    tar xf "fpc-${FPC_VERSION}.x86_64-linux.tar" && cd "fpc-${FPC_VERSION}.x86_64-linux" && \
    # Automate the install of FPC by replacing the interactive questions
    sed -i 's/if yesno "Install documentation"; then/if false; then/' install.sh && \
    sed -i 's/if yesno "Install demos"; then/if false; then/' install.sh && \
    sed -i 's/ask "Install prefix (\/usr or \/usr\/local) " PREFIX/echo "Installing to (\/usr)" /' install.sh && \
    # Install FPC
    ./install.sh && \
    cd /opt && \
    # Install Pasdoc and clean
    wget --quiet "https://github.com/pasdoc/pasdoc/releases/download/v${PASDOC_VERSION}/pasdoc-${PASDOC_VERSION}-linux-x86_64.tar.gz" && \
    tar xf "pasdoc-${PASDOC_VERSION}-linux-x86_64.tar.gz" && cp /opt/pasdoc/bin/pasdoc /usr/local/bin/ && \
    # Cleanup
    rm -R pasdoc "pasdoc-${PASDOC_VERSION}-linux-x86_64.tar.gz" "fpc-${FPC_VERSION}.x86_64-linux" "fpc-${FPC_VERSION}.x86_64-linux.tar" && \
    apt clean && rm -R /var/lib/apt/lists/*
